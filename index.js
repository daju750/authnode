const express = require('express');
const cors = require('cors');
const path = require('path');
const {dbConnection} = require('./db/config')
require('dotenv').config();

const app = express();

//CORS
app.use(cors());

dbConnection();

//Directorio Publico
app.use(express.static('public'));

//Lectura y Parceo
app.use(express.json());

//Rutas
app.use('/api/auth',require('./routes/auth'))

app.get('*',(req,res)=>{
    res.sendFile(path.resolve(__dirname,'public/index.html'))
})

app.listen(process.env.PORT,()=>{
    console.log('Servidor corriendo en puesto 4000')
})
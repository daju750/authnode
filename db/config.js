const mongoose = require('mongoose')

const dbConnection = async() => {

    const URI = process.env.DB_CNN;

    try{

        await mongoose.connect(URI, {
    
            useNewUrlParser: true, 
            useUnifiedTopology: true,
            
        });
        console.log("Conexion Exitosa")
    }catch(error){
        console.log(error);
    }
    
}

module.exports = {
    dbConnection
}